import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import netCDF4
from tqdm import tqdm
import h5py
import datetime
from matplotlib.backends.backend_pdf import PdfPages

import logging

# Example: tropics_selection.py <path_to_data> <file_name>

# Creating the variables
lat = np.zeros((835, 77)) * np.nan
long = np.zeros((835, 77)) * np.nan
sza = np.zeros((835, 77)) * np.nan
qa_value = np.zeros((835, 77)) * np.nan
low_qa_pixels = np.zeros((835, 77)) * np.nan
cloud_fraction_ozone = np.zeros((835, 77)) * np.nan
high_cloud_fraction_pixels = np.zeros((835, 77)) * np.nan
to3 = np.zeros((835, 77)) * np.nan
to3_prec = np.zeros((835, 77)) * np.nan
trop3 = np.zeros((835, 77)) * np.nan
trop3_prec = np.zeros((835, 77)) * np.nan
o3p_subcolumns = np.zeros((835, 77, 6)) * np.nan
o3p_subcolumns_prec = np.zeros((835, 77, 6)) * np.nan

file_path = sys.argv[1]

file_name = sys.argv[2]

fref = h5py.File(file_path, 'r')

print('Reading the file', fref, file_name)
logging.info('Reading the file %s, with this name %s', fref, file_name)

# Setting up the logger file
s_date = datetime.datetime.now().strftime('%Y%m%d') 
logging.basicConfig(filename=f'/usr/people/dipede/projects/TROPOMI/scripts/output_tropics_complete_preselection_{file_name}_{s_date}.log', level=logging.DEBUG, filemode='w', format='%(asctime)s %(name)s - %(levelname)s - %(message)s')
logging.info('Start log file (written in UTC time).')



print('Start loop over to3 and tropo3')
logging.info('Start loop over to3 and tropo3')


for i in tqdm(range(835)):
    for j in range(77):

        # Preselection on the qa-value of the pixels (discard all the pixels with qa value < 0.5)
        if(fref['PRODUCT/qa_value'][0, i, j] > 50):

            # Preselection on the cloud fraction of the pixels (discard all the pixels with cloud fraction > 0.5)
            if(fref['PRODUCT/SUPPORT_DATA/INPUT_DATA/cloud_fraction_crb_ozone_window'][0, i, j] < 0.5):     

                # Select the tropic area on the latitude
                if (fref['PRODUCT']['latitude'][0, i, j] > -24) & (fref['PRODUCT']['latitude'][0, i, j] < 24):

                    logging.info('Scanline %s, ground pixel %s', i, j)

                    lat[i, j] = fref['PRODUCT']['latitude'][0,i,j]
                    long[i, j] = fref['PRODUCT']['longitude'][0,i,j]
                    sza[i, j] = fref['PRODUCT/SUPPORT_DATA/GEOLOCATIONS/solar_zenith_angle'][0,i,j]
                    qa_value[i, j] = fref['PRODUCT/qa_value'][0,i,j]
                    cloud_fraction_ozone[i, j] = fref['PRODUCT/SUPPORT_DATA/INPUT_DATA/cloud_fraction_crb_ozone_window'][0,i,j]


                    to3[i, j] = fref['PRODUCT']['ozone_total_column'][0,i,j]
                    to3_prec[i, j] = fref['PRODUCT']['ozone_total_column_precision'][0,i,j]
                    trop3[i, j] = fref['PRODUCT']['ozone_tropospheric_column'][0,i,j]       
                    trop3_prec[i, j] = fref['PRODUCT']['ozone_tropospheric_column_precision'][0,i,j]                  

                else:
                    continue


            else:
                logging.info('cloud fraction in the ozone window too high: %s', fref['PRODUCT/SUPPORT_DATA/INPUT_DATA/cloud_fraction_crb_ozone_window'][0, i, j])
                high_cloud_fraction_pixels[i, j] = fref['PRODUCT/SUPPORT_DATA/INPUT_DATA/cloud_fraction_crb_ozone_window'][0, i, j]
                continue

        else: 
            logging.info('qa_value too low: %s', fref['PRODUCT/qa_value'][0, i, j])
            low_qa_pixels[i, j] = fref['PRODUCT/qa_value'][0, i, j]
            continue



print('Start loop over ozone profile')
logging.info('Start loop over ozone profile')

# Now loop over the subcolumns for the ozone profile selection
for i in tqdm(range(835)):
    for j in range(77):

        # Preselection on the qa-value of the pixels (discard all the pixels with qa value < 0.5)
        if(fref['PRODUCT/qa_value'][0, i, j] > 50):

            # Preselection on the cloud fraction of the pixels (discard all the pixels with cloud fraction > 0.5)
            if(fref['PRODUCT/SUPPORT_DATA/INPUT_DATA/cloud_fraction_crb_ozone_window'][0, i, j] < 0.5):     

                # Select the tropic area on the latitude
                if (fref['PRODUCT']['latitude'][0, i, j] > -24) & (fref['PRODUCT']['latitude'][0, i, j] < 24):     

                    # Loop over the subcolumns
                    for sub in range(6):

                        logging.info('Scanline %s, ground pixel %s, subcolumn %s', i, j, sub)

                        o3p_subcolumns[i, j, sub] = fref['PRODUCT']['ozone_profile_subcolumns'][0, i, j, sub]
                        o3p_subcolumns_prec[i, j, sub] = fref['PRODUCT']['ozone_profile_subcolumns_precision'][0, i, j, sub]
                else:
                    continue

            else:
                continue

        else: 
            continue
 
# Write the output
outfile_name = '/usr/people/dipede/projects/TROPOMI/data/{}_{}_qa0.5_cloudfr0.5_complete_preselection_tropics.h5'.format(s_date, file_name)
print('output filename: ' + outfile_name)
logging.info('Output filename: %s', outfile_name)

h5f = h5py.File(outfile_name, 'w')

h5f.create_dataset('latitude', data=(lat[:,:]).astype(np.float32))
h5f.create_dataset('longitude', data=(long[:,:]).astype(np.float32))
h5f.create_dataset('sza', data=(sza[:,:]).astype(np.float32))
h5f.create_dataset('qa_value', data=(qa_value[:,:]).astype(np.float32))
h5f.create_dataset('low_qa_pixels', data=(low_qa_pixels[:,:]).astype(np.float32))
h5f.create_dataset('cloud_fraction_crb_ozone_window', data=(cloud_fraction_ozone[:,:]).astype(np.float32))
h5f.create_dataset('high_cloud_fraction_crb_ozone_window', data=(high_cloud_fraction_pixels[:,:]).astype(np.float32))

h5f.create_dataset('ozone_total_column', data=(to3[:,:]).astype(np.float32))
h5f.create_dataset('ozone_total_column_precision', data=(to3_prec[:,:]).astype(np.float32))
h5f.create_dataset('ozone_tropospheric_column', data=(trop3[:,:]).astype(np.float32))
h5f.create_dataset('ozone_tropospheric_column_precision', data=(trop3_prec[:,:]).astype(np.float32))
h5f.create_dataset('ozone_profile_subcolumns', data=(o3p_subcolumns[:,:,:]).astype(np.float32))
h5f.create_dataset('ozone_profile_subcolumns_precision', data=(o3p_subcolumns_prec[:,:,:]).astype(np.float32))

h5f.close()

print('End script')
logging.info('End of the script')
